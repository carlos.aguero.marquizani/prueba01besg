window.onload = () => {
    console.log("Cargando")
}

const inicio = () => {
    const data = {
        nombre: document.getElementById('nombre').value,
        edad: document.getElementById('edad').value,
        sexo: document.getElementById('sexo').value,
    }
    console.log(data)
    var myInit = {
        method: 'POST',
        body: JSON.stringify(data),
        headers:{
            'Content-Type': 'application/json'
        }
    }

    var url = 'http://127.0.0.1:3000/inicio'
    fetch(url, myInit)
    .then(response => response.text())
    .then(data => console.log(data))
}
