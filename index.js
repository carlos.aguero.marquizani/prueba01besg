/*
Nombres: 
        - Carlos Aguero
        - Vicente Perez
Carrera:
        - Informática m/ ciberseguridad
Docente: 
        - Paulina Gonzalez

*/
//Frases no permitidas de forma manual
const noPermitido = [
    "parseInt",
    "document.write",
    "eval",
    "isNaN",
    "unescape",
    "escape",
    "parseFloat",
    "parseInt",
    "eval",
    "isNaN",
    "onload",
    "alert",
    "script",
    "<",
    ">",
    "onload",
    "=",
    "(",
    ")",
    '"',
    '{',
    '}',
    '[',
    ']'

]

//################################################
//Instalaciones
const express = require('express');
const app = express();
es6Renderer = require('express-es6-template-engine')
app.use(express.json())
app.use(express.urlencoded())
const cors = require('cors')

app.use(cors())
const bodyParser = require('body-parser')
app.use(bodyParser.json())

app.engine('html', es6Renderer);
app.set('views', 'views');
app.set('view engine', 'html');

const { Configuration, OpenAIApi } = require("openai");
const configuration = new Configuration({
  apiKey: "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",
});
const openai = new OpenAIApi(configuration);

//################################################
//Segurización de forma manual 

sustituir = (texto,reemplazar) =>{

    while(texto.includes(reemplazar)){
        console.log(`Hemos detectado código malicioso, Se ha reemplazado ${reemplazar} en ${texto}`)
        texto = texto.replace(reemplazar,'')
    }
    return texto
}

limpiarReq = (data) =>{
    
    noPermitido.forEach(word => {
        var nombre = data.nombre
        var edad = data.edad 
        var sexo = data.sexo
        nombre = sustituir(nombre,word)
        edad = sustituir(edad,word)
        sexo = sustituir(sexo,word)
        data = {
            nombre,
            edad,
            sexo
        }
    })
    return data
    
}


//################################################
//Rutas

app.get('/welcome',(req , res)=>{
    res.send('Bienvenido al sistema de Jubilicaiones de Chile')
})

app.get('/inicio',(req , res)=>{
    res.sendFile('./views/inicio.html', {
        root: __dirname
    });
})

app.get('/login',(req , res)=>{
    res.sendFile('./views/login.html', {
        root: __dirname
    });
})

app.get('/respuesta',(req , res)=>{
    res.sendFile('./views/respuesta.html', {
        root: __dirname
    });

})

app.get('/manual',(req , res)=>{
    res.sendFile('./views/manual.html', {
        root: __dirname
    });

})

app.get('/openai',(req , res)=>{
    res.sendFile('./views/openai.html', {
        root: __dirname
    });

})

app.post('/respuesta', (req,res) => {
    console.log(req.body)
    res.render('respuesta',{locals: {
        nombre : req.body.nombre,
        edad : req.body.edad,
        sexo : req.body.sexo
    }})
    
})

app.post('/manual', (req,res) => {

    const toSend = limpiarReq({
        nombre: req.body.nombre,
        edad: req.body.edad,
        sexo: req.body.sexo
    })
    console.log( toSend)
    res.render('manual',{
                                locals: toSend
                            });
})

app.post('/openai',async (req,res) => {
    console.log(req.body.nombre)
    console.log(req.body.edad)
    console.log(req.body.sexo)
    duda = `contiene lenguaje 
    javascript en la siguiente expresion?
    "${req.body.nombre}" "${req.body.edad}" "${req.body.sexo}". Responde true o false en minusculas`
    console.log(duda);
    const completion = await openai.createCompletion({
        model: "text-davinci-003",
        prompt: duda
      });
      console.log(completion.data.choices[0].text);
    if(completion.data.choices[0].text.trim() == "true"){
        res.render('openai',{locals: {
            nombre : "contiene caracteres invalidos",
            edad : "contiene caracteres invalidos",
            sexo : "contiene caracteres invalidos"
        }})
    }else{
        res.render('openai',{locals: {
            nombre : req.body.nombre,
            edad : req.body.edad,
            sexo : req.body.sexo}})
    }  
    
})

app.listen(3000, () => {
  console.log('Escuchando por el puerto 3000');
});
